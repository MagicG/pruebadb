package com.tis.convocatorias.pruebas.probando.Prueba;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface PostulantRepository extends JpaRepository<Postulant, String> {

    @Query("select item from Postulant item where item.ci = :ci")
    Optional<Postulant> FindByCi(@Param("ci") String ci);

    @Modifying
    @Transactional
    @Query("update Postulant item set item.name = :name where item.ci = :ci")
    void UpdateByCi(@Param("ci") String ci, @Param("name") String name);
}
