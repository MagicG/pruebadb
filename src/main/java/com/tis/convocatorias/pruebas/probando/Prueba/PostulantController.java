package com.tis.convocatorias.pruebas.probando.Prueba;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulantController {

    @Autowired
    private PostulantService postulantService;

    @RequestMapping("/postulants")
    public List<Postulant> getAllPostulants() {
        return postulantService.getAllPostulants();
    }

    @RequestMapping("/postulants/{id}")
    public Postulant getPostulant(@PathVariable String id) {
        return postulantService.getPostulant(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/postulants")
    public void addPostulant(@RequestBody Postulant postulant) {
        postulantService.addPostulant(postulant);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/postulants/{id}")
    public void updatePostulant(@RequestBody Postulant postulant, @PathVariable String id) {
        postulantService.updatePostulant(id, postulant);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/postulants/{id}")
    public void deletePostulant(@PathVariable String id) {
        postulantService.deletePostulant(id);
    }
}
