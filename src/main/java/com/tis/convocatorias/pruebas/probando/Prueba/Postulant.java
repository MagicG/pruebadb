package com.tis.convocatorias.pruebas.probando.Prueba;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "Person")
public class Postulant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @Column(name = "id", updatable = false, nullable = false)
    private long id;
    @Column
    private String ci;
    @Column
    private String name;
    @Column(name = "surname")
    private String lastName;
    @Column
    private String address;
    @Column(name = "phone")
    private int phoneNumber;
    @Column
    private String email;

    public Postulant() {
    }

    public Postulant(String ci, String name, String lastName, String address, int phoneNumber, String email) {
        this.ci = ci;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
