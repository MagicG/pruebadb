package com.tis.convocatorias.pruebas.probando.Prueba;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostulantService {

    @Autowired
    private PostulantRepository postulantRepository;


//    private List<Postulant> postulants = new ArrayList<>(Arrays.asList(
//            new Postulant("1", "Pepe", "Villegas", "Av. Oquendo 1232", 78945612,
//                    "pepe@gmail.com", "LCO-ADM", "Laboratorio de Computo"),
//            new Postulant("2", "Mauricio", "Vargas", "Av. Ingavi 1462", 71972612,
//                    "mauri@gmail.com", "LCO-ADM-HW", "Laboratorio de Computo"),
//            new Postulant("3", "Rosa", "Martinez", "Av. Oquendo 3514", 73249814,
//                    "rosita@gmail.com", "LCO-ADM-SW", "Laboratorio de Computo")
//    ));

    public List<Postulant> getAllPostulants() {
        //return postulants;
        List<Postulant> postulants = new ArrayList<>();
        postulantRepository.findAll()
                .forEach(postulants::add);
        return postulants;
    }

    public Postulant getPostulant(String ci) {
        //return postulants.stream().filter(t -> t.getId().equals(id)).findFirst().get();
//        String aux = id.toString();
//        return postulantRepository.getOne(id);

        return postulantRepository.FindByCi(ci)
                .orElse(null);
    }


    public void addPostulant(Postulant postulant) {

        postulantRepository.save(postulant);
    }

    public void updatePostulant(String ci, Postulant postulant) {
        postulantRepository.UpdateByCi(ci, postulant.getName());

    }

    public void deletePostulant(String id) {
        //postulants.removeIf(p -> p.getId().equals(id));
        postulantRepository.deleteById(id);
    }
}
